package com.dataaccess.cliente;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.dataaccess.webservicesserver.NumberConversion;
import com.dataaccess.webservicesserver.NumberConversionSoapType;

public class WS01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NumberConversionSoapType servicio = new NumberConversion().getNumberConversionSoap();
		int numA = 23;
		int numB= 78;
		String dolar = servicio.numberToDollars(new BigDecimal(numA));
		String letras = servicio.numberToWords(new BigInteger("45"));
		System.out.println("Convertir "+numA+" a dolares: \n"+dolar);
		System.out.println("Convertir " +numB+" a letras: \n"+letras);
	}

}
