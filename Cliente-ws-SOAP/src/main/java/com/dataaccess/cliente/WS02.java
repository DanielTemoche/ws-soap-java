package com.dataaccess.cliente;

import org.tempuri.Calculator;
import org.tempuri.CalculatorSoap;

public class WS02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CalculatorSoap servicio = new Calculator().getCalculatorSoap();
		int numA = 23;
		int numB= 78;
		int suma = servicio.add(numA, numB);
		int multiplica = servicio.multiply(numA, numB);
		int division = servicio.divide(numA, numB);
		System.out.println("La suma de: "+numA+" + "+numB+" es igual a: \n"+suma);
		System.out.println("La multiplicacion de: "+numA+" * "+numB+" es igual a: \n"+multiplica);
		System.out.println("La division de: "+numA+" / "+numB+" es igual a: \n"+division);
		
	}

}
