package com.dataaccess.cliente;
import java.util.List;

import uasi.ws_gestdocente.ArrayOfClaseAsiDepto;
import uasi.ws_gestdocente.ClaseAsiDepto;
import uasi.ws_gestdocente.PubGestdocente;
import uasi.ws_gestdocente.PubGestdocenteSoap;

public class WS03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PubGestdocenteSoap servicio = new PubGestdocente().getPubGestdocenteSoap();
		
		ArrayOfClaseAsiDepto asignaturas = servicio.wsasidepto("C", "2011-12", "B142", "");
		List<ClaseAsiDepto> datos = asignaturas.getClaseAsiDepto();
		for (ClaseAsiDepto claseAsiDepto : datos) {
			System.out.println(claseAsiDepto.getCodasi()+"\t"+claseAsiDepto.getCodest()+"\t"+claseAsiDepto.getNomasi()+"\t\t"+claseAsiDepto.getNomest());
		}
	}

}
